#include <string.h>
#include <errno.h>

#include "slist.h"
#include "pages.h"
#include "inode.h"
#include "directory.h"
#include "util.h"
#include "bitmap.h"
#include "storage.h"

void 
directory_init() {

    //mark out our inode pages
    void* bm = get_pages_bitmap();
    if(bitmap_get(bm, INODE_PAGE)) {
        return;
    }

    int size = bytes_to_pages(sizeof(inode) * PAGE_COUNT);
    printf("INODE PAGES: %d, %ld\n", size, sizeof(inode));
    for(int ii = 0; ii < size; ii++) {
        bitmap_put(bm, INODE_PAGE + ii, 1);
    }

    //lets make the root
    inode* root = pages_get_page(INODE_PAGE);
    root->refs = 0;
    root->mode = 040755;
    root->size = 0;

}

int 
search_page(int pnum, const char* name) {
    dirent* ents = pages_get_page(pnum);

    for(int ii = 0; ii < 64; ii++) {
        if(ents[ii].inuse && strcmp(name, ents[ii].name) == 0) {
            printf("SEARCH_RETURNING_FOUND: %d\n", ents[ii].inum);
            return ents[ii].inum;
        }
    }
    return -1;
}

int 
directory_lookup(inode* dd, const char* name) {
    //look for an item in dd and return its inode
    int* indirect = pages_get_page(dd->iptr);
    int pgs = bytes_to_pages(dd->size);

    if(streq(name, "")) {
        return 0;
    }
    
    if(!S_ISDIR(dd->mode)) {
        printf("DIRLOOKUP: nodir %d\n", dd->mode);
        return -ENOTDIR;
    }
    
    if(!(dd->mode & 00400)) {
        printf("DIRLOOKUP: %d, %d\n", dd->mode, dd->mode & 00400);
        return -EACCES;
    }
    
    int rv = -1;
    for(int ii = 0; ii < pgs; ii++) {
        if(ii < 2) {
            // direct pointers
            rv = search_page(dd->ptrs[ii], name);
        }
        else {
            // search the indirect page
            rv = search_page(indirect[ii - 2], name);
        }

        if(rv >= 0) {
            printf("DIRLOOKUP: rvil %d\n", rv);
            return rv;
        }
    }
    printf("DIRLOOKUP: rvool %d\n", rv);
    return rv;
}

int 
tree_lookup(const char* path) {
    // return the inum associated with the path
    // if it doesnt exist return error from dir_lookup

    // starting at the root inode;
    int inum = 0;
    inode* cur_inode = get_inode(0);    

    // create an slist from the path
    slist* plist = s_split(path, '/');
    slist* pcur = plist;


    while(1) {
        //get the current inum for the current position in the path
        inum = directory_lookup(cur_inode, pcur->data);
        if(inum < 0) {
            return inum;
        }

        //move to sub-directory for next check
        cur_inode = get_inode(inum);

        // if we reached the end of the path break
        if(pcur->next == 0) {
            break;
        }
        // otherwise go to next path entry
        else {
            pcur = pcur->next;
        }
    }

    // free out slist
    s_free(plist);

    return inum;
}

int
find_next_free(dirent* ents) {
    for(int ii = 0; ii < 64; ii++) {
        if(!ents[ii].inuse) {
            return ii;
        }
    }
    return -1;
}

int 
directory_put(inode* dd, const char* name, int inum) {
    //put a new entry in the directory
    if(directory_lookup(dd, name) != -1) {
        //item already exists
        return -1;
    }
    //grow the inode to fit the new entry
    int rv = grow_inode(dd, dd->size + sizeof(dirent));
    if(rv == -1) {
        return -1;
    }

    int pgs = bytes_to_pages(dd->size);
    int* indirects = pages_get_page(dd->iptr);;
    dirent* ents;
    int free;
    
    for(int ii = 0; ii < pgs; ii++) {
        if(ii < 2) {
            ents = pages_get_page(dd->ptrs[ii]);
            free = find_next_free(ents);
        }
        else {
            ents = pages_get_page(indirects[ii - 2]);
            free = find_next_free(ents);
        }
        //put the new entry in
        if(free != -1) {
            //ents[free].name = strcpy(ents[free].name, naie);
            for(int jj = 0; jj < DIR_NAME; jj++) {
                ents[free].name[jj] = name[jj];
            }
            ents[free].inum = inum;
            ents[free].inuse = 1;
            return 0;
        }
    }
    return -1;
    
}

int
rem_first_item(dirent* ents, const char* name) {
    for(int ii = 0; ii < 64; ii++) {
//        printf("ENTNAME:   %s\n", ents[ii].name);
//        printf("ENTINUM:   %d\n", ents[ii].inum); 
//        printf("ENTIUSE:   %d\n", ents[ii].inuse);

        if(ents[ii].inuse && strcmp(ents[ii].name, name) == 0) {
            ents[ii].inuse = 0;
            return ii;
        }
    }
}

int 
directory_delete(inode* dd, const char* name) {
    //remove an entry from the directory
    //remove the entry
    int rpage = -1;
    int rindex = -1;
    int* indirects = pages_get_page(dd->iptr);
    dirent* ents;
    int pgs = bytes_to_pages(dd->size + 1) - 1;
   
    //printf("pg:   %d\n", pgs);
    //find the last page
    dirent move;
    if(pgs < 2) {
        ents = pages_get_page(dd->ptrs[pgs]);
    }
    else {
        ents = pages_get_page(indirects[pgs - 2]);
    }

    //look at the values in the last page and record the last entry
    for(int ii = 63; ii >= 0; ii--) {
        
//        printf("ii:   %d\n", ii);
        
        if(ents[ii].inuse) {
            //copy it into the temp move item;
            for(int jj = 0; jj < DIR_NAME; jj++) {
                move.name[jj] = ents[ii].name[jj];
            }
            move.inum = ents[ii].inum;
            move.inuse = ents[ii].inuse;

            //set the copied value to unused on the page
            ents[ii].inuse = 0;
            break;
        }
    }
    //printf("MOVENAME:   %s\n", move.name);
    //printf("MOVEINUM:   %d\n", move.inum); 
    //printf("MOVEIUSE:   %d\n", move.inuse);

    //find the ent we actually want to remove 
    for(int ii = 0; ii <= pgs; ii++) {
        rpage = ii;

        //shrink return if our move item is the one to be removed
        if(strcmp(move.name, name) == 0) {
            int bail = shrink_inode(dd, dd->size - sizeof(dirent));
            return 0;
        }

        if(ii < 2) {
            ents = pages_get_page(dd->ptrs[ii]);
        }
        else {

            ents = pages_get_page(indirects[ii - 2]);
        }

        rindex = rem_first_item(ents, name);
    }
    //put the moved item back in
    int rv = directory_put(dd, move.name, move.inum); 
    if(rv != 0) {
        return rv;
    }

    //shrink to the new size
    rv = shrink_inode(dd, dd->size - 2 * sizeof(dirent));
    if(rv < 0) {
        return -1;
    }
//    printf("DIR_DEL: %d\n", rv);
    return 0;
}

slist*
add_page_entries(slist* items, dirent* ents) {
    for(int ii = 0; ii < 64; ii++) {
        if(ents[ii].inuse) {
            items = s_cons(ents[ii].name, items);
        }
    }
    return items;
}

slist*
directory_list(const char* path) {
    //make an slist of the items in the directory
    //tree lookup
    int inum = tree_lookup(path);
    inode* dd = get_inode(inum);

    dirent* ents;
    int* indirects = pages_get_page(dd->iptr);
    //then list all items;
    slist* items = 0;
    
    int pgs = bytes_to_pages(dd->size);
    for(int ii = 0; ii < pgs; ii++) {
        if(ii < 2) {
            items = add_page_entries(items, pages_get_page(dd->ptrs[ii]));
        }
        else {
            items = add_page_entries(items, pages_get_page(indirects[ii - 2]));
        }
    }
    return items;
}

void
print_directory(inode* dd) {
    //print the directory, debug
    printf("Directory\n");
    print_inode(dd);
    //print all the entries
    int pgs = bytes_to_pages(dd->size);
    dirent* ents;
    int* indirects = pages_get_page(dd->iptr);
    for(int ii = 0; ii < pgs; ii++) {
        if(ii < 2) {
            ents = pages_get_page(dd->ptrs[ii]);
        }
        else {
            ents = pages_get_page(indirects[ii - 2]);
        }

        for(int jj = 0; jj < 64; jj++) {
            if(ents[jj].inuse) {
                printf("Name: %s\n Inum: %i\n Inuse: %i\n", ents[jj].name, ents[jj].inum, ents[jj].inuse);
            }
        }
    }
}

