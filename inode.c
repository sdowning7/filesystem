#include <stdio.h>

#include "pages.h"
#include "inode.h"
#include "storage.h"
#include "util.h"
void 
print_inode(inode* node) {
    printf("Refs:   %d\n", node->refs);
    printf("Mode:   %o\n", node->mode);
    printf("Size:   %d\n", node->size);
    printf("DPtr:   %d\n", node->ptrs[0]);
    printf("DPtr2:  %d\n", node->ptrs[1]);
    printf("IPtr:   %d\n", node->iptr);
}

inode* 
get_inode(int inum) {
    //find the inode and return its pointer
    inode* nodes = pages_get_page(INODE_PAGE);
    return &(nodes[inum]);
}

int alloc_inode() {
    //fund an unused inode  and return the number
    inode* nodes = pages_get_page(INODE_PAGE);
    for(int ii = 0; ii < PAGE_COUNT; ii++) {
        if(nodes[ii].mode == 0) {
            nodes[ii].ptrs[0] = alloc_page();
            nodes[ii].refs = 1;
            return ii;
        }
    }
    return -1;
}
void 
free_inode(inode* node) {
    //mark the inode as free by setting the mode to 0
    
    //free the pages by shrinking to size 0
    shrink_inode(node, 0);
    //set everything else to 0;
    node->refs = 0;
    node->mode = 0;
    node->size = 0;
    node->ptrs[0] = 0;
    node->ptrs[1] = 0;
    node->iptr = 0;
}

int 
grow_inode(inode* node, int size) {
    //allocate more data blocks for the inode up to size
    if(size < node->size) {
        return -1;
    }

    int opgs = bytes_to_pages(node->size);
    int npgs = bytes_to_pages(size);
    int* indirect = pages_get_page(node->iptr);

    node->size = size;

    for(int ii = opgs; ii < npgs; ii++) {
        //do the direct pointers
        if(ii < 2) {
            if((node->ptrs[ii] = alloc_page()) == -1) {
                return -1;
            }
        }
        //do the indirect pointer
        else {
            if(node->iptr == 0) {
                if((node->iptr = alloc_page()) == -1) {
                    return -1;
                }
                indirect = pages_get_page(node->iptr);
            }
            if((indirect[ii - 2] = alloc_page()) == -1) {
                return -1;
            }          
        }
    }
    return node->size;
}

int
shrink_inode(inode* node, int size) {
    //free data blocks from the end of the inode 
    if(size > node->size) {
        return -1;
    }

    int opgs = bytes_to_pages(node->size);
    int npgs = bytes_to_pages(size);
    int* indirect = pages_get_page(node->iptr);

    node->size = size;

    for(int ii = npgs; ii < opgs; ii++) {
        if(ii < 2) {
            free_page(node->ptrs[ii]);
            node->ptrs[ii] = 0;
        }
        else {
            free_page(indirect[ii - 2]);
            indirect[ii - 2] = 0;
        }
    }
    if(npgs < 2) {
        free_page(node->iptr);
        node->iptr = 0;
    }
//    printf("SHRINK: %d\n", node->size);    
    return node->size;
}

int 
inode_get_pnum(inode* node, int fpn) {
    //get the page number of the file page number
    if(fpn > bytes_to_pages(node->size)) {
        return -1;
    }
    if(fpn < 2 && fpn >= 0) {
        return node->ptrs[fpn];
    }
    else if(fpn < PAGE_COUNT) {
        int* indirect = pages_get_page(node->iptr);
        return indirect[fpn - 2];
    }

    return -1;
}
