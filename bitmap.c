#include <stdio.h>

#include "bitmap.h"

int 
bitmap_get(void* bm, int ii) {
    char* map = (char*) bm;
    int bii = ii / 8;
    int rii = ii % 8;

    return (map[bii] >> rii) & 1;
}

void 
bitmap_put(void* bm, int ii, int vv) {
    char* map = (char*) bm;
    int bii = ii / 8;
    int rii = ii % 8;

    if(vv == 1) {
        map[bii] = map[bii] | (vv << rii);
    }
    if(vv == 0) {
        map[bii] = map[bii] & ~(1 << rii);
    }   
}

void 
bitmap_print(void* bm, int size) {
    for(int ii = 0; ii < size / 8; ii++) {
        printf("%02x", ((unsigned char *) bm) [ii]);
    }
    printf("\n");
}
