#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "slist.h"
#include "storage.h"
#include "pages.h"
#include "inode.h"
#include "util.h"
#include "bitmap.h"
#include "directory.h"

void
storage_init(const char* path) {

    //lets initilize the file.
    pages_init(path);

    //lets initiaize the root directory
    directory_init();

    /*
    void* bitmap = get_pages_bitmap();
    bitmap_print(bitmap, PAGE_COUNT);
//    */
}

int storage_stat(const char* path, struct stat* st) {
    int rv = tree_lookup(path);
//    printf("lookup_result: %d\n", rv);
    
    if(rv == -1) {
        return -ENOENT;
    }
    if(rv < 0) {
        return rv;
    }

    inode* n = get_inode(rv);
    
    st->st_ino = rv;
    st->st_nlink = n->refs;
    st->st_mode = n->mode;
    st->st_size = n->size;
    st->st_uid = getuid();
    st->st_atim = n->times[0];
    st->st_mtim = n->times[1];
    st->st_ctim = n->times[2];
    return 0;
}

int storage_read(const char* path, char* buf, size_t size, off_t offset) {
    struct stat st;
    int rv = storage_stat(path, &st);
    if(rv != 0) {
        return rv;
    }

    if(!(st.st_mode & 00400)) {
        return -EBADF;
    }


    inode* n = get_inode(st.st_ino);
    
    
    if(S_ISREG(st.st_mode) || S_ISLNK(st.st_mode)) {
        //update timestamps
        struct timespec new[2];
        new[1] = st.st_mtim;
        rv = clock_gettime(CLOCK_REALTIME, &new[0]); 
        if(rv != 0) {
            return rv;
        }
        rv = storage_set_time(path, new);
        if(rv != 0) {
            return rv;
        }

        int pg;
        char* file;
        int* indirect = pages_get_page(n->iptr);
        int index;
        int count = 0;
        //read across all buf pages up to the size or inode size
        for(int ii = offset; ii < offset + size; ii++) {
            pg = (bytes_to_pages(ii + 1) - 1);
            if(ii >= n->size) {
                break;
            }

            if(pg < 2) {
                file = pages_get_page(n->ptrs[pg]);
            }
            else {
                file = pages_get_page(indirect[pg - 2]);
            }
            index = ii - 4096 * pg;
            buf[ii - offset] = file[index];
            count++;

        }
        return count;
    }

    return -EINVAL;
}

int
storage_write(const char* path, const char* buf, size_t size, off_t offset) {
    struct stat st;
    int rv = storage_stat(path, &st);
    if(rv != 0) {
        return rv;
    }

    if(!(st.st_mode & 00200)) {
        return -EBADF;
    }

    if(S_ISREG(st.st_mode) || S_ISLNK(st.st_mode)) {
        
        //update out reading timestamps
        rv = storage_set_time(path, 0);
        if(rv != 0) {
            return rv;
        }

        //make sure the node is big enough
        inode* n = get_inode(st.st_ino);
        rv = grow_inode(n, size + offset);
        if(rv == -1) {
            return -EDQUOT;
        }

        int pg;
        char* file;
        int* indirect = pages_get_page(n->iptr);
        int index;
        int count = 0;
        //write across all pages from buf
        for(int ii = offset; ii < offset + size; ii++) {
            pg = (bytes_to_pages(ii + 1) - 1);
            if(ii >= n->size) {
                break;
            }

            if(pg < 2) {
                //write on one of the direct pages
                file = pages_get_page(n->ptrs[pg]);
            }
            else {
                //write on one of the indirect pages
                file = pages_get_page(indirect[pg - 2]);
            }
            //actually do the copy and account for pages
            index = ii - 4096 * pg;
            file[index] = buf[ii - offset];
            count ++;
        }
        return count;
    }
    return -EINVAL;

}

int    
storage_truncate(const char *path, off_t size) {
    // truncate the file
    // get the inum from the path
    int inum = tree_lookup(path);
    if(inum < 0) {
        return inum;
    }

    // get inode from inum
    inode* node = get_inode(inum);
    
    int rv = -1;
    
    if(!(node->mode & 00200)) {
        return -EACCES;
    }
    
    if(node->size == size) {
        return 0;
    }
    // truncate the inode
    else if(node->size < size) {
        rv = grow_inode(node, size);
    }
    else if (node->size > size) {
        rv = shrink_inode(node, size);
    }
    if(rv < 0) {
        return rv;
    }
    // update the timestamps for the inode
    rv = storage_set_time(path, 0);
    return rv;
}

int
storage_mknod(const char* path, int mode) {

    char* name = malloc(DIR_NAME);
    char* npath = malloc(strlen(path));
    npath = strcpy(npath, path);

    char* last = strrchr(npath, '/');
    
    name = strcpy(name, last + 1);

    if(strlen(name) > DIR_NAME) {
        free(name);
        free(npath);
        return -ENAMETOOLONG;
    }

    last[1] = 0;
    //printf("TEST: %s\n", npath); 
    
    int rv = tree_lookup(npath);
    if(rv < 0) {
        free(name);
        free(npath);
        return -ENOENT;
    }
    
    inode* dd = get_inode(rv);

    if(!(dd->mode & 00200)) {
        return -EACCES;
    }

    if(!S_ISDIR(dd->mode)) {
        free(name);
        free(npath);
        return -ENOTDIR;
    }
    
    int inum = alloc_inode();
    if(inum == -1) {
        free(name);
        free(npath);
        return -EDQUOT;
    }
    
    inode* n = get_inode(inum);
    n->mode = mode;

    //print_directory(dd);
    //printf("%s\n", name);
    rv = directory_put(dd, name, inum);
    if (rv != 0) {
        return rv;
    }
    
    rv = storage_set_time(path, 0);

    //print_directory(dd);

    free(name);
    free(npath);
    return rv;
}

int    
storage_unlink(const char* path) {
    int rv = -1;

    char* name = malloc(DIR_NAME);
    char* npath = malloc(strlen(path));
    npath = strcpy(npath, path);

    char* last = strrchr(npath, '/');

    name = strcpy(name, last + 1);

    if(strlen(name) > DIR_NAME) {
        free(name);
        free(npath);
        return -ENAMETOOLONG;
    }

    last[1] = 0;

    //check the mode
    rv = tree_lookup(npath);
    if(rv < 0) {
        free(name);
        free(npath);
        return -ENOENT;
    }
    
    //check dd mode
    inode* dd = get_inode(rv);
    if(!S_ISDIR(dd->mode)) {
        free(name);
        free(npath);
        return -ENOTDIR;
    }
    
    if(!(dd->mode & 00200)) {
        return -EACCES;
    }

    //update directory timestamp
    rv = storage_set_time(npath, 0);
    if(rv != 0) {
        return rv;
    }
        
    rv = tree_lookup(path);
    if (rv < 0) {        
        free(name);
        free(npath);
        return -ENOENT;
    }

    //check n mode
    inode* n = get_inode(rv);
    if(!S_ISREG(n->mode)) {
        free(name);
        free(npath);
        return -EISDIR;
    }
    
    if(!(n->mode & 00200)) {
        return -EACCES;
    }

    //update inode timestamp
    struct timespec new[2];
    new[0] = n->times[0];
    new[1] = n->times[1];
    rv = storage_set_time(path, new);
    if(rv != 0) {
        return rv;
    }

    //check the links and free the node if there are no more
    n->refs--;
    if(n->refs <= 0) {
        free_inode(n);
    }

    //remove the directory entry
    print_directory(dd);
    rv = directory_delete(dd, name);
    if(rv < 0) {
        free(name);
        free(npath);
        return -EISDIR;
    }
    print_directory(dd);

    free(name);
    free(npath);
    return 0;
}

int    
storage_link(const char *from, const char *to) {
    int rv = -1;
    
    char* name = malloc(DIR_NAME);
    char* tpath = malloc(strlen(to));
    tpath = strcpy(tpath, to);

    char* last = strrchr(tpath, '/');

    name = strcpy(name, last + 1);

    if(strlen(name) > DIR_NAME) {
        free(name);
        free(tpath);
        return -ENAMETOOLONG;
    }

    last[1] = 0;

    // check if the to directory exists
    int dnum = tree_lookup(tpath);
    if(dnum < 0) {
        free(name);
        free(tpath);
        return -ENOENT;
    }
    // check if the to file exists
    rv = tree_lookup(to);
    if(rv >= 0) {
        // return an error if it does
        free(name);
        free(tpath);
        return -EEXIST;
    }
    // check if the from file exists
    int inum = tree_lookup(from);
    if(inum < 0) {
        free(name);
        free(tpath);
        return -ENOENT;
    }

    inode* dd = get_inode(dnum);
    if(!(dd->mode & 00200)) {
        return -EACCES;
    }

    // get the from file
    inode* node = get_inode(inum);

    // increment its refs
    node->refs++;

    // add a new entry in the to directory
    rv = directory_put(dd, name, inum);

    return rv;
}

int    
storage_rename(const char *from, const char *to) {
    int rv = -1;
    
    //check the mode on the first
    struct stat fst;
    rv = storage_stat(from, &fst);
    if(rv != 0) {
        return rv;
    }

    if(!S_ISREG(fst.st_mode)) {
        return -EISDIR;
    }
    
    if(!(fst.st_mode & 00200)) {
        return -EACCES;
    }

    //make the to one if we need it, if not check the mode
    struct stat tst;
    rv = storage_stat(to, &tst);
    //does it exist?
    if(rv != 0) {   
        //make it if not
        rv = storage_mknod(to, 0100644);
        if(rv != 0) {
            return rv;
        }
    } 
    else {
        if(!(tst.st_mode & 00200)) {
            return -EINVAL;
        }
    }
    
    char* buf = malloc(fst.st_size);
    
    //read all the data into a buffer
    rv = storage_read(from, buf, fst.st_size, 0);
    if(rv < 0) {
        free(buf);
        return rv;    
    }
    //and write it to the new file
    rv = storage_write(to, buf, fst.st_size, 0);
    if(rv < 0) {
        free(buf);
        return rv;
    }
    //delete the old file
    rv = storage_unlink(from);

    return rv;
}

int    
storage_set_time(const char* path, const struct timespec ts[2]) {
    int rv = tree_lookup(path);
    if(rv < 0) {
        return -ENOENT;
    }
        
    inode* n = get_inode(rv);

    if(!(n->mode & 00200)) {
        return -EINVAL;
    }

    if(ts == 0) {
        rv = clock_gettime(CLOCK_REALTIME, &n->times[0]);
        n->times[1] = n->times[0];
        n->times[2] = n->times[0];
        return rv;
    } 

    n->times[0] = ts[0];
    n->times[1] = ts[1];
    rv = clock_gettime(CLOCK_REALTIME, &n->times[2]);
    return rv;
}

slist* 
storage_list(const char* path) {
    return directory_list(path);;
}


int
storage_readlink(const char* path, char* buf, size_t size) {
    struct stat st;
    int rv = storage_stat(path, &st);
    if(rv >= 0 && !S_ISLNK(st.st_mode)) {
        return -EINVAL;
    }

    // read from the symlink into the buffer
    rv = storage_read(path, buf, size, 0);
    if(rv < 0) {
        return rv;
    }
    
    // update access time
    struct timespec ts[2];
    rv = clock_gettime(CLOCK_REALTIME, &ts[0]);
    if(rv < 0) {
        return rv;
    }
    ts[1] = st.st_mtim;

    rv = storage_set_time(path, ts); 

    return 0;
}

int
storage_symlink(const char* to, const char* from) {
    // from is the name of the link
    // to is the target of the link
   
    int rv = -1;
    
    char* name = malloc(DIR_NAME);
    char* fpath = malloc(strlen(from));
    fpath = strcpy(fpath, from);

    char* last = strrchr(fpath, '/');

    name = strcpy(name, last + 1);

    if(strlen(name) > DIR_NAME) {
        free(name);
        free(fpath);
        return -ENAMETOOLONG;
    }

    last[1] = 0;

    // check if the link directory exists
    int dnum = tree_lookup(fpath);
    if(dnum < 0) {
        free(name);
        free(fpath);
        return -ENOENT;
    }
    // check if the link file already exists
    rv = tree_lookup(from);
    if(rv >= 0) {
        // return an error if it does
        free(name);
        free(fpath);
        return -EEXIST;
    }
    // check if the target file exists
    int inum = tree_lookup(to);
    if(inum < 0) {
        free(name);
        free(fpath);
        return -ENOENT;
    }

    
    // mknod for the new link
    rv = storage_mknod(from, 0120644);
    if(rv < 0) {
        return rv;
    }

    // write link data to the link node
    rv = storage_write(from, to, strlen(to), 0); 
    if(rv < 0) {
        return rv;
    }
    return 0;
}

