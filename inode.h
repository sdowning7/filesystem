// based on cs3650 starter code

#ifndef INODE_H
#define INODE_H

#include "pages.h"
#include <sys/time.h>

typedef struct inode {
    int refs; // reference count
    int mode; // permission & type
    int size; // bytes of the data in the file/directory
    int ptrs[2]; // direct pointers
    int iptr; // single indirect pointer
    struct timespec times[3]; // timestamps of the inode
                                //0: last access
                                //1: last modification
                                //2: last status change
} inode;

void print_inode(inode* node);
inode* get_inode(int inum);
int alloc_inode();
void free_inode(inode* node);
int grow_inode(inode* node, int size);
int shrink_inode(inode* node, int size);
int inode_get_pnum(inode* node, int fpn);

#endif
